# chayuta-web-template-v2
a starting point for chayuta web page development


## for local development
- `yarn install`
- `yarn debug`
- debug in vscode or simply a browser on http://localhost:3030/example

## dev instructions
- edit your code in src/example and put assets in static/assets/example
- try to not change styles outside of the 'example' foders, especially the airfleet folder that is going to be phased out