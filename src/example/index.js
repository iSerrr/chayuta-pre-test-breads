import './example.scss';

import $ from 'jquery';

import Navbar from '../../airfleet/scripts/theme/components/navbar';
import Header from '../../airfleet/scripts/theme/components/header';
import AnimateOnScroll from '../../airfleet/scripts/theme/components/aos';
import TestimonialsSlider from "../_partials/testimonials-slider";
import Router from '../../airfleet/scripts/theme/components/router';
import ContactDialog from '../_partials/contact-dialog';


$(document).ready(async () => {
    console.log("ready");
    const router = new Router([
        new Navbar(),
        new Header(),
        new AnimateOnScroll({disable: false}),
        new TestimonialsSlider({selector: '.block_testimonials.multi .block_testimonials_slider'}),
        new ContactDialog()
    ]); 

    router.bootstrap();

    $("header .header__button").addClass("header__button--visible");
});
