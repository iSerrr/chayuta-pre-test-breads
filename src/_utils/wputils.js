const ARTICLES_QUERY = `
query GET_ARTICLES($tags: [String])
{
  posts(where: {tagSlugIn: $tags, categoryNotIn: 13}, last: 50) { 
    edges {
      node {
        databaseId
        slug
        title
        uri
        excerpt
        author {
          node {
            name
            nickname
            avatar {
              url
            }
          }
        }
        tags {
          nodes {
            name
            id
          }
        }
        featuredImage {
          node {
            sourceUrl
            mediaDetails {
              file
            }
          }
        }
        dateGmt
        categories {
          nodes {
            name
            databaseId
          }
        }
      }
    }
  }
}
`;

const ARTICLE_QUERY = `
query GET_ARTICLE($id: Int)
{
  posts(where: {id: $id}) {
    edges {
      node {
        databaseId
        slug
        title
        uri
        excerpt
        content
        author {
          node {
            name
            nickname
            avatar {
              url
            }
          }
        }
        tags {
          nodes {
            name
            id
          }
        }
        featuredImage {
          node {
            sourceUrl
            mediaDetails {
              file
            }
          }
        }
        dateGmt
        categories {
          nodes {
            name
          }
        }
      }
    }
  }
}
`;

const RECOMMENDATIONS_QUERY = `
  {
    recommendations(last: 20) {
      edges {
        node {
          content
          title
          uri
          recommendationMeta {
            title
            recommender
            link
            image {
                sourceUrl
                mediaDetails {
                  file
                }
            }
          }
        }
      }
    }
  }
`;

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

export default class WPUtils {
  constructor() {
  }

  async getArticles(tags) {
    try {
      let req = await fetch('/wpgql', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          query: ARTICLES_QUERY,
          variables: { tags }
        })
      });

      if (!req.ok)
        throw req;

      let list = (await req.json()).data.posts;

      let res = (list.edges || []).map(v => {
        let node = v.node || {};
        return {
          id: node.databaseId,
          slug: node.slug,
          excerpt: node.excerpt,
          title: node.title,
          link: node.link,
          image: (((node.featuredImage || {}).node || {}).mediaDetails || {}).file,
          tags: (node.tags || {}).nodes || [],
          categories: (node.categories || {}).nodes || [],
          author: (node.author || {}).node || {}
        };
      });
      return res;
    } catch (ex) {
      console.error(ex);
      return [];
    }
  }

  async getArticle(id) {
    id = id | 0;
    try {
      let req = await fetch('/wpgql', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          query: ARTICLE_QUERY,
          variables: { id }
        })
      });

      if (!req.ok)
        throw req;

      let list = ((await req.json()).data.posts || {}).edges || [];

      let article = undefined;
      if (list.length > 0) {
        let v = list[0];
        let node = v.node || {};
        article = {
          id: node.databaseId,
          slug: node.slug,
          excerpt: node.excerpt,
          date_gmt: node.dateGmt,
          title: node.title,
          link: node.link,
          content: node.content,
          image: (((node.featuredImage || {}).node || {}).mediaDetails || {}).file,
          tags: (node.tags || {}).nodes || [],
          categories: (node.categories || {}).nodes || [],
          author: (node.author || {}).node || {}
        };

        try {
          let d = article.date_gmt;
          article.utcDate = new Date(Date.UTC(d.substring(0, 4), d.substring(5, 7)-1, d.substring(8, 10)));
          article.date_display = new Intl.DateTimeFormat().format(article.utcDate);
          article.date_display = new Intl.DateTimeFormat('he-il', {
            month: 'short',
            day: 'numeric',
            year: 'numeric'
          }).format(article.utcDate);
        } catch (ignore) {
          console.error(ignore);
        }
      }
      return article;
    } catch (ex) {
      console.error(ex);
      return undefined;
    }
  }

  async getRecommendations() {
    try {
      let req = await fetch('/wpgql', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          query: RECOMMENDATIONS_QUERY
        })
      });

      if (!req.ok)
        throw req;

      let list = (await req.json()).data.recommendations;

      let res = (list.edges || []).map(v => {
        let node = v.node || {};
        let meta = node.recommendationMeta || {};
        return {
          content: node.content,
          recommender: meta.recommender,
          title: meta.title,
          link: meta.link,
          avatar: ((meta.image || {}).mediaDetails || {}).file
        };
      });

      try { shuffleArray(res); } catch(ignore){}
      return res;
    } catch (ex) {
      console.error(ex);
      return [];
    }
  }
}
