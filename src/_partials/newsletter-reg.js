import $ from 'jquery'
export default class NewsletterReg {
    constructor() {
        this.selector = '.newsletter-reg';
    }

    async registerEmail(el) {
        try {
            let email = el.val();

            if (this._busy) return;

            clearTimeout(this.infoto);
            $(el).closest("form").find("div:nth-child(2)").removeClass("on info");

            this._busy = true;
            const data = new URLSearchParams();
            data.append("email", email);
            let http_res = await fetch('https://chat.chayuta.com/admin/leads/reg.jsp', {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                method: 'post',
                body: data,
            });

            let res = await http_res.json();
            if (!res || res.status !== "ok")
                throw res && res.error ? {desc: res.error} : "failed to register";

            $(el).val("").closest("form").find("div:nth-child(2)")
                .text("ברכות! נרשמת בהצלחה")
                .addClass("info on");
            this.infoto = setTimeout(()=>{
                $(el).closest("form").find("div:nth-child(2)").removeClass("on");
            }, 5000);
        } catch(ex) {
            let errorText = (ex && ex.desc) || "ניתן לנסות שוב מאוחר יותר.";
            errorText = "מצטערים, לא ניתן להרשם במועד זה. " + errorText;
            console.error(ex);
            $(el).closest("form").find("div:nth-child(2)")
                .text(errorText)
                .removeClass("info").addClass("on");
        } finally {
            this._busy = false;
        }
    }

    // eslint-disable-next-line class-methods-use-this
    bootstrap() {
        $(this.selector).closest("form").on("submit", e => {
            e.preventDefault();
            this.registerEmail($(e.target).find("input"))
        });
    }
}
