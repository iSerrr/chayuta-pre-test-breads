import $ from 'jquery'
import Swiper from 'swiper/js/swiper.min.js';
import WPUtils from '../_utils/wputils';

export default class TestimonialsSlider {
    constructor(options) {
        options = options || {};
        this.selector = options.selector || '.block_testimonials_slider';
        this.parent_selector = this.selector + ' ' + '.block_testimonials_slider_wrapper';
        this.sliders = [].slice.call(document.querySelectorAll(`${this.selector}`));
        this.recommendations = options.recommendations;

        this.options = {
            slidesPerView: 'auto',
            spaceBetween: 25,
            loop: true,
            centeredSlides: true,
            watchOverflow: true,
            simulateTouch: false,
            breakpoints: {
                480: {
                    spaceBetween: 30,
                },
                768: {
                    slidesPerView: 2,
                    centeredSlides: false,
                    spaceBetween: 36,
                },
                1440: {
                    slidesPerView: 3,
                    centeredSlides: false,
                    spaceBetween: 36,
                },
            },
            pagination: {
                el: '.chayuta-swiper-pagination',
                type: 'bullets',
                clickable: true,
                bulletClass: 'chayuta-swiper-pagination-bullet',
                bulletActiveClass: 'chayuta-swiper-pagination-bullet-active',
                modifierClass: 'chayuta-swiper-pagination-',
            },
            navigation: {
                nextEl: '.block_testimonials_nav .swiper-button-next',
                prevEl: '.block_testimonials_nav .swiper-button-prev',
            },

            containerModifierClass: 'block_testimonials_slider_',
            slideClass: 'block_testimonials_slider_slide',
            slideActiveClass: 'block_testimonials_slider_slide-active',
            slideDuplicateActiveClass: 'block_testimonials_slider_slide-duplicate-active',
            slideVisibleClass: 'block_testimonials_slider_slide-visible',
            slideDuplicateClass: 'block_testimonials_slider_slide-duplicate',
            slideNextClass: 'block_testimonials_slider_slide-next',
            slideDuplicateNextClass: 'block_testimonials_slider_slide-duplicate-next',
            slidePrevClass: 'block_testimonials_slider_slide-prev',
            slideDuplicatePrevClass: 'block_testimonials_slider_slide-duplicate-prev',
            wrapperClass: 'block_testimonials_slider_wrapper',
            ...options.slider
        };
    }

    // eslint-disable-next-line class-methods-use-this
    bootstrap() {
        if (this.sliders.length === 0) {
            return;
        }

        this.loadCards();
    }

    async loadCards() {
        let recommendations = this.recommendations || await new WPUtils().getRecommendations();
        let parent = $(this.parent_selector).empty();
        recommendations.forEach((v, k) => {
            parent
                .append(
                    $("<div/>").addClass("block_testimonials_slider_slide")
                        .append(
                            $("<div/>").addClass("box-testimonial")
                                .append($("<a/>").attr({href: v.link, target: "_blank"}).addClass("c-link box-testimonial__link"))
                                .append($("<div/>").addClass("box-testimonial__quotes")
                                    .append('<img src="/assets/quotes-icon-' + ((k % 2) + 1) + '.svg" class="c-image" alt="" height="33" width="46" />')
                                )
                                .append($("<div/>").addClass("box-testimonial__body")
                                    .append($("<blockquote/>").html(v.content))
                                )
                                .append($('<div class="box-testimonial__head"/>')
                                    .append($('<div class="box-testimonial__image"/>')
                                        .append($("<img/>").addClass("c-image").attr({src: v.avatar ? "/uploads/" + v.avatar : undefined}))
                                    )
                                    .append($('<div class="box-testimonial__author"/>')
                                        .append($("<h5/>").text(v.recommender))
                                        .append($("<p/>").text(v.title))
                                    )
                                    .append('<div class="box-testimonial__icon box-testimonial__icon--facebook"></div>')
                                )
                        )
                );
        });
        this.createSlides();
    }

    createSlides() {
        this.sliders.forEach(slider => {
            const loopedSlides = $(slider).find('.chayuta-swiper-slide').length;
            let autoplay = {};

            if (slider.dataset.autoplay === 'true') {
                autoplay = {
                    delay: parseInt(slider.dataset.autoplayDelay * 1000, 10),
                };
            } else
                autoplay = undefined;

            // eslint-disable-next-line no-new 
            new Swiper(slider, {
                ...this.options,
                loopedSlides,
                autoplay,
            });
        });
    }
}
