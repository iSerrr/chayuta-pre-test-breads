import $ from 'jquery'
export default class ContactDialog {
    constructor() {
        this.selector = '.contactFormExec';
    }

    getCookieValue(cookieName) {
        const parts = `; ${document.cookie}`.split(`; ${cookieName}=`);
        if (parts.length === 2)
            return parts.pop().split(';').shift();
        return undefined;
    }

    // eslint-disable-next-line class-methods-use-this
    bootstrap() {
        $("#contact-dialog, #contact-dialog .modal-close-button i")
            .on("click", e => {
                this.hideContactForm();
            });
        $("#contact-dialog .popup").on("click", e => {
            e.stopPropagation();
        });

        $("#contact-dialog form").on("submit", e => {
            //adjust lead source
            let items = (window.location.pathname || "").split(":");
            if (items.length >= 2 && items[0] === "/article") {
                if (items.length >= 3 && (items[2] * 1 + "") === items[2]) {
                    $("#contact-dialog form").prop("action", "https://chat.chayuta.com/GenLead/" + items[2]);
                }

                $('#contact-dialog form input[name="extra"]').val("article " + items[1]);
            }

            //collect extra data
            $('#contact-dialog form input[name="extra_data"]').val("");
            try {
                let qpGet = () => { return undefined };
                if (window.URLSearchParams) {
                    let usp = new window.URLSearchParams(window.location.search);
                    qpGet = usp.get.bind(usp);
                }

                let extra_data = {
                    hubspotutk: this.getCookieValue("hubspotutk"),
                    utm_source: qpGet("utm_source"),
                    utm_medium: qpGet("utm_medium"),
                    utm_campaign: qpGet("utm_campaign"),
                    utm_term: qpGet("utm_term"),
                    utm_content: qpGet("utm_content")
                }
                Object.keys(extra_data).forEach(k => {
                    if (!extra_data[k])
                        delete extra_data[k];
                });
                $('#contact-dialog form input[name="extra_data"]').val(JSON.stringify(extra_data));
            } catch (ex) {
                console.error("failed to collect extra data", ex);
            }

            //validate phone number format
            //e.preventDefault();

            return true;
        });

        $(this.selector).on("click", e => {
            e.preventDefault();

            $("#contact-dialog").show();
            window.requestAnimationFrame(() => {
                $("#contact-dialog").addClass("on");
                $("#contact-dialog input[name=name]").focus();
            });
        });
    }

    hideContactForm() {
        $("#contact-dialog").removeClass("on");
        setTimeout(() => {
            $("#contact-dialog").hide();
        }, 1000);
    }
}
