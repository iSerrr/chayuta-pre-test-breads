import $ from 'jquery'

export default class YoutubePopup {
    constructor(id, buttonSelector, youtubeId, autoPlay) {
        this.options = {
            id,
            buttonSelector,
            youtubeId,
            autoPlay
        }
        this.selector = buttonSelector;
    }

    // eslint-disable-next-line class-methods-use-this
    bootstrap() {
        $(this.options.buttonSelector).click(e => {
            e.preventDefault();
            $("#" + this.options.id).remove();
            let popup = this.buildPopup();
            $("body").append(popup);
            window.requestAnimationFrame(()=>{
                popup.addClass("on");
            });
        });
    }

    buildPopup() {
        return $('<div class="modal-blocker"/>').attr("id", this.options.id)
            .append(
                $('<div class="video-positioner">')
                    .append(
                        $('<div class="video-wrapper">')
                            .append($('<iframe/>').attr({
                                src: "https://www.youtube.com/embed/" + this.options.youtubeId + "?autoplay=" + (this.options.autoPlay ? 1 : 0),
                                allow: "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture",
                                allowfullscreen: true
                            }))
                    )
            ).click(e => {
                $(e.target).removeClass("on");
                setTimeout(() => {
                    $(e.target).remove();
                }, 1000);
            });
    }
}
