import AOS from 'aos';

export default class AnimateOnScroll {
  constructor(options) {
    this.selector = '';
    this.options = {
      disable: window.innerWidth < 768,
      once: true,
      duration: 800,
      ...options
    };

    this.initAos();
  }

  initAos() {
    AOS.init(this.options);
  }
}
