const path = require('path');
const glob = require('glob-all')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const PurgecssPlugin = require('purgecss-webpack-plugin');
//const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const CSS_WHITELIST = [
	'::-webkit-scrollbar-thumb',
	'class-whitelister',
	'aos-init',
	'aos-animate',
	'header--sticky',
	'header--visible',
	'animate',
	'mobile-nav-visible',
	'header__button--visible',
	'on',
	'info',
	"head",
	"wp-block-quote",
	"video-positioner",
	"video-wrapper",
	"reversed"
];

const CSS_WHITELIST_PATTERNS = [
	/.*aos.*/,
	/pagination-bullet.*/,
	/chayuta-swiper-pagination.*/
]

module.exports = env => {
	env = env || {};
	const publicUrl = env.publicUrl || "https://chayuta.com";
	const gqlUrl = env.gqlUrl || "http://wpprod.chayuta.com/cms/graphql";
	const gqlHost = env.gqlHost || "wpprod.chayuta.com";

	return {
		mode: "development",
		//devtool: 'inline-source-map',
		devtool: 'source-map',
		devServer: {
			port: 3030,
			contentBase: './dist',
			compress: true,
			disableHostCheck: true,
			proxy: {
				"/wpgql": {
					target: gqlUrl,
					headers: {
						'Host': gqlHost
					},
					changeOrigin: true
				},
				"/uploads": {
					target: gqlUrl.replace("graphql", "wp-content"),
					headers: {
						'Host': gqlHost
					},
					changeOrigin: true
				},
			}
		},
		optimization: {
			splitChunks: {
				// include all types of chunks
				chunks: 'all'
			}
		},
		entry: {
			example: './src/example/index.js',
		},
		output: {
			filename: 'bundle.[id].[hash].js',
			path: path.resolve(__dirname, 'dist')
		},
		module: {
			rules: [
				{
					test: /\.(sa|sc|c)ss$/,
					use: [
						{
							loader: MiniCssExtractPlugin.loader,
							options: {
								hmr: process.env.NODE_ENV === 'development',
							},
						},
						'css-loader?url=false',
						'sass-loader',
					],
				},
				{
					test: /\.(js)$/,
					exclude: /node_modules/,
					use: ['babel-loader']
				}
			],
		},
		resolve: {
			extensions: ['*', '.js']
		},
		plugins: [
			new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),
			new MiniCssExtractPlugin({
				filename: '[name].[hash].css',
			}),
			new HtmlWebpackPlugin({
				chunks: ['example'],
				filename: 'example/index.html',
				template: 'src/example/index.html'
			}),
			new CopyWebpackPlugin({
				patterns: [
					{
						from: 'static',
						globOptions: {
							ignore: [
								'**/originals/*'
							]
						}
					}
				],
			}),
			
			new PurgecssPlugin({
				paths: glob.sync([
					`${path.join(__dirname, 'src/example')}/**\/*.html`,
					`${path.join(__dirname, 'src/_partials')}/**\/*.html`
				]),
				only: ["example"],
				whitelist: CSS_WHITELIST,
				whitelistPatterns: CSS_WHITELIST_PATTERNS
			}),
			
			//new BundleAnalyzerPlugin()
		],
	}
};